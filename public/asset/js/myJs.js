$(document).ready(function () {

    //    my js

    // delete  item

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var deletetableId;
    var href;
    $('.closs-item').on('click', function () {
        event.preventDefault();
        deletetableId = $(this).attr('data-id');
        href = $(this).attr('href');
        $('.closs-this').attr('data-get',deletetableId)
    })

    $('.closs-this').on('click',function () {
        event.preventDefault();
        $(this).attr('data-get');
        // console.log($(this).attr('data-get'));
        $.ajax({
            type: $(this).data('method'),
            url: href,
            data: {
                id: deletetableId
            },
            success: (data) => {
                // console.log(data);
                $('#deleteModal').modal('hide');
                $('#' + data['id']).remove();
            }
        })
    });

    var user;
    var nameinput =  $("input#name");
    var namemail=  $("input#email");
    var current;
    $('[data-target="#openModal"]').on('click',function () {
        current =$(this);
        user = jQuery.parseJSON($(this).attr('data-user'));
        $('#openModal').modal('show');
        nameinput.val(user.name);
        namemail.val(user.email);
        $("#select_country option[value=" + user.country_id + "]").prop('selected', true);
    })
//    triger click
    $('body').on('click','[data-click]',function () {
        var click = $(this).data('click');
        $('#'+click).trigger('submit');
    })
    $('#trigger_click').submit(function () {
        var url = $(this).parents('form').attr('action').replace(/.$/,user.id)
        $.ajax({
            type:'PUT',
            url:url,
            data:{
                name: nameinput.val(),
                email: namemail.val(),
                country_id: $('#select_country').find(":selected").val(),
                country: $('#select_country').find(":selected").text()
            },
            success:function (data) {
                var datanormal = jQuery.parseJSON(data);
                console.log(datanormal)
                $('#'+datanormal.id +' .getname').text(datanormal.name );
                $('#'+datanormal.id +' .getmail').text(datanormal.email );
                $('#'+datanormal.id +' .getcountry').text(datanormal.country.name );
                current.attr('data-user',data)
            }
        })

    })
    //upload hidden button
    $(document).on('change','.upload_excel',()=>{
        $("button.excel_btn").removeClass('d-none');
    })

})

