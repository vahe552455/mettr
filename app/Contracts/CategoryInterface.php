<?php
namespace App\Contracts;


Interface CategoryInterface {
    public function all();
    public function store($data);
    public  function deleteCategory($id);
    public  function edit($id);
}