<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\CountryInterface;
use App\Contracts\UserInterface;
use App\Http\Requests\UserRegister;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    private $countryRepo;


    use RegistersUsers;

    public function __construct(CountryInterface $model , UserInterface $user)
    {
        $this->middleware('guest');
        $this->countryRepo = $model;
        $this->userRepo = $user;
    }


    public function showRegistrationForm( )
        {
        $countries = $this->countryRepo->all();
        return view('auth.register',compact('countries'));
    }

    public function register(UserRegister $request)
    {
        $user = $this->userRepo->create($request->validated());
        $this->guard()->login($user);
//        $this->countryRepo->update($request->country_id , ['user_id' => auth()->user()->id]);
//        event(new Registered($user = $this->create($request->all())));
//        $this->guard()->login($user);
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }
}
