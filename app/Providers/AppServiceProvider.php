<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Contracts\UserInterface',
            'App\Repositories\UserRepo'
        );

       $this->app->bind('App\Contracts\CountryInterface',
            'App\Repositories\CountryRepo'
        );
        $this->app->bind('App\Contracts\CategoryInterface',
            'App\Repositories\CategoryRepo'
        );
        $this->app->bind('App\Contracts\NewsInterface',
            'App\Repositories\NewsRepo'
        );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
